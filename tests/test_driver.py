import asyncio
import inspect
import os
import sys
import unittest
import logging

from tornado.ioloop import IOLoop
from tornado.testing import AsyncTestCase, gen_test

try:
    from pgpydriver.driver import PostgreDriver
except ImportError:
    import sys
    sys.path.append(os.path.join(os.path.dirname(__file__), '../'))
    from pgpydriver.driver import PostgreDriver


class pycolor:
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    END = '\033[0m'
    BOLD = '\038[1m'
    UNDERLINE = '\033[4m'
    INVISIBLE = '\033[08m'
    REVERCE = '\033[07m'


class TestLogger(logging.StreamHandler):

    def emit(self, record):
        record.msg = pycolor.RED + record.getMessage() + pycolor.END
        super().emit(record)


def func_name(func):
    async def _func_name(*args, **kwargs):
        # print(func.__name__)
        if inspect.iscoroutinefunction(func):
            return await func(*args, **kwargs)
        else:
            return func(*args, **kwargs)
    return _func_name


if not os.environ.get("postgreHost", None):
    os.environ["postgreHost"] = "localhost"
    os.environ["postgreDBname"] = "test"
    os.environ["postgreUser"] = "test"
    os.environ["postgrePassword"] = "test"
    os.environ["postgrePort"] = "55543"


class TestDriver(unittest.TestCase):

    def setUp(self):
        self.driver: PostgreDriver = PostgreDriver()
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.stream_handler = TestLogger()
        self.logger.addHandler(self.stream_handler)
        logging.root.handlers = []

    def tearDown(self):
        super().tearDown()
        self.logger.removeHandler(self.stream_handler)

    def test_a_init(self):
        self.logger.info('init')
        self.driver.connect()
        sql = "drop table if exists test"
        self.driver.execute(sql)
        sql = """
        create table if not exists test
        (
            id         numeric(2)
            ,name       varchar(20)
            ,constraint test_pkey PRIMARY KEY(id)
        )
        """
        result = self.driver.execute(sql)
        sql = "insert into test (id,name) values (%s,%s)"
        result2 = self.driver.execute(sql, (1, 'testuser'))
        self.driver.commit()
        self.assertEqual(-1, result)
        self.assertEqual(1, result2)

    def test_b_select(self):
        self.logger.info('select test')
        from decimal import Decimal
        self.driver.connect()
        sql = "select * from test"
        datas: dict = self.driver.execute(sql, cnt=1)
        for key, value in datas.items():
            if isinstance(value, Decimal):
                datas[key] = int(value)
        self.assertEqual({'id': 1, 'name': 'testuser'}, datas, 'select ng')

    def test_c_delete(self):
        self.logger.info('delete test')
        self.driver.connect()
        sql = "delete from test where id = %s"
        result = self.driver.execute(sql, (1,))
        self.driver.commit()
        self.assertEqual(1, result, 'delete ng')

    def test_d_insert(self):
        self.logger.info('insert test')
        self.driver.connect()
        sql = "insert into test(id,name) values (%s,%s)"
        result = self.driver.execute(sql, (2, 'test2'))
        self.driver.commit()
        self.assertEqual(1, result, 'insert ng')

    def test_e_update(self):
        self.logger.info('update info')
        self.driver.connect()
        sql = "update test set name = 'test2update' where id = %s"
        result = self.driver.execute(sql, (2,))
        self.driver.commit()
        self.assertEqual(1, result, 'update ng')

    def test_f_select(self):
        self.logger.info('select2 test')
        from decimal import Decimal
        self.driver.connect()
        sql = "select * from test where id = 2"
        datas: dict = self.driver.execute(sql, cnt=1)
        for key, value in datas.items():
            if isinstance(value, Decimal):
                datas[key] = int(value)
        self.assertEqual({'id': 2, 'name': 'test2update'}, datas, 'select ng')


if __name__ == "__main__":
    unittest.main()
