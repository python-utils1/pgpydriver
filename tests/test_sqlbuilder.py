from pgpydriver.sqlbuilder import SqlBuilder
import unittest
import logging
from .test_utils import TestLogger


class TestSqlBulder(unittest.TestCase):

    def setUp(self):
        super(TestSqlBulder, self).setUp()
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.stream = TestLogger()
        self.logger.addHandler(self.stream)

    def tearDown(self):
        super(TestSqlBulder, self).tearDown()
        self.logger.removeHandler(self.stream)

    def test_a_select(self):
        self.logger.info('select')
        sql = SqlBuilder()
        sql.select('*').From('test')
        sql_ = sql()
        self.assertEqual('select * from test', sql_)

    def test_b_select_where(self):
        self.logger.info('select where')
        sql = SqlBuilder()
        sql.select('*').From('test').where('id = %(id)s').And('name = %(name)s')
        sql_ = sql()
        self.assertEqual(
            'select * from test where id = %(id)s and name = %(name)s', sql_)

    def test_c_select_list(self):
        self.logger.info('select column list')
        sql = SqlBuilder()
        sql.select(['id', 'name']).From('test')
        sql_ = sql()
        self.assertEqual('select id,name from test', sql_)

    def test_d_select_inner_join(self):
        self.logger.info('select inner join on only')
        sql = SqlBuilder()
        sql.select('*').From('test').inner_join('test2', 'test.id = test2.id')
        sql_ = sql()
        self.assertEqual(
            'select * from test inner join test2 on test.id = test2.id', sql_)

    def test_e_select_left_join(self):
        self.logger.info('select left join on only')
        sql = SqlBuilder()
        sql.select('*').From('test').left_join('test2', 'test.id = test2.id')
        sql_ = sql()
        self.assertEqual(
            'select * from test left join test2 on test.id = test2.id', sql_)

    def test_f_select_inner_join_and(self):
        self.logger.info('select inner join on and and')
        sql = SqlBuilder()
        sql.select('*').From('test').inner_join('test2',
                                                'test.id = test2.id').And('test.name = test2.name')
        sql_ = sql()
        self.assertEqual(
            'select * from test inner join test2 on test.id = test2.id and test.name = test2.name', sql_)

    def test_g_select_left_join_and(self):
        self.logger.info('select left join on and and')
        sql = SqlBuilder()
        sql.select('*').From('test').left_join('test2',
                                               'test.id = test2.id').And('test.name = test2.name')
        sql_ = sql()
        self.assertEqual(
            'select * from test left join test2 on test.id = test2.id and test.name = test2.name', sql_)
