import asyncio
import inspect
import os
import sys
import unittest
import logging

from tornado.ioloop import IOLoop
from tornado.testing import AsyncTestCase, gen_test

try:
    from pgpydriver.driver import PostgreDriver
except ImportError:
    import sys
    sys.path.append(os.path.join(os.path.dirname(__file__), '../'))
    from pgpydriver.driver import PostgreDriver

logging.root.handlers = []


def func_name(func):
    async def _func_name(*args, **kwargs):
        # print(func.__name__)
        if inspect.iscoroutinefunction(func):
            return await func(*args, **kwargs)
        else:
            return func(*args, **kwargs)
    return _func_name


if not os.environ.get("postgreHost", None):
    os.environ["postgreHost"] = "localhost"
    os.environ["postgreDBname"] = "test"
    os.environ["postgreUser"] = "test"
    os.environ["postgrePassword"] = "test"
    os.environ["postgrePort"] = "55543"


class pycolor:
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    END = '\033[0m'
    BOLD = '\038[1m'
    UNDERLINE = '\033[4m'
    INVISIBLE = '\033[08m'
    REVERCE = '\033[07m'


class TestLogger(logging.StreamHandler):

    def emit(self, record):
        record.msg = pycolor.RED + record.getMessage() + pycolor.END
        super().emit(record)


class TestAsyncDriver(AsyncTestCase):

    def tearDown(self):
        super(TestAsyncDriver, self).tearDown()
        del self.driver
        self.logger.removeHandler(self.stream_handler)

    def setUp(self):
        super(TestAsyncDriver, self).setUp()
        self.driver: PostgreDriver = PostgreDriver()
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.stream_handler = TestLogger()
        self.logger.addHandler(self.stream_handler)

    @gen_test
    async def test_a_init_async(self):
        self.logger.info("""テスト用テーブル作成""")
        sql = "drop table if exists test"
        await self.driver.execute_async(sql)
        sql = """
        create table if not exists test
        (
            id         numeric(2)
            ,name       varchar(20)
            ,constraint test_pkey PRIMARY KEY(id)
        )
        """
        result = await self.driver.execute_async(sql)
        sql = "insert into test (id,name) values (%s,%s)"
        result2 = await self.driver.execute_async(sql, (1, 'testuser'))
        self.driver.commit()
        self.assertEqual(-1, result, "Async Create Table Error")
        self.assertEqual(1, result2, "Async Insert Test Data Error")

    @gen_test
    async def test_b_select_async(self):
        self.logger.info('select async test')
        from decimal import Decimal
        await self.driver.connect_async()
        sql = "select * from test"
        datas: dict = await self.driver.execute_async(sql, cnt=1)
        for key, value in datas.items():
            if isinstance(value, Decimal):
                datas[key] = int(value)
        self.assertEqual({'id': 1, 'name': 'testuser'}, datas, 'select ng')

    @gen_test
    async def test_c_delete_async(self):
        self.logger.info('delete async test')
        await self.driver.connect_async()
        sql = "delete from test where id = %s"
        result = await self.driver.execute_async(sql, (1,))
        self.driver.commit()
        self.assertEqual(1, result, 'delete ng')

    @gen_test
    async def test_d_insert_async(self):
        self.logger.info('insert async test')
        await self.driver.connect_async()
        sql = "insert into test(id,name) values (%s,%s)"
        result = await self.driver.execute_async(sql, (2, 'test2'))
        self.driver.commit()
        self.assertEqual(1, result, 'insert ng')

    @gen_test
    async def test_e_update_async(self):
        self.logger.info('update async test')
        await self.driver.connect_async()
        sql = "update test set name = 'test2update' where id = %s"
        result = await self.driver.execute_async(sql, (2,))
        self.driver.commit()
        self.assertEqual(1, result, 'update ng')

    @gen_test
    async def test_f_select_async(self):
        self.logger.info('select async test')
        from decimal import Decimal
        await self.driver.connect_async()
        sql = "select * from test"
        datas = await self.driver.execute_async(sql)
        _datas = []
        for data in datas:
            for key, value in data.items():
                if isinstance(value, Decimal):
                    data[key] = int(value)
            _datas.append(data)
        check_data = [
            {'id': 2, 'name': 'test2update'}
        ]
        self.assertEqual(check_data, _datas, 'select ng')
